#ifndef MQTTROOMCONTROL_HPP
#define MQTTROOMCONTROL_HPP

#include "MQTTNode.hpp"
#include "firmata/client.hpp"
#include "firmata/serial_port.hpp"
#include "firmata/debounce.hpp"
#include <vector>
#include <ola/DmxBuffer.h>
// #include <ola/Logging.h>
#include <ola/client/StreamingClient.h>

class MQTTRoomControl : public MQTTNode
{
private:
    static const std::string signal_reset;
    static const std::chrono::milliseconds debounce_duration;
    static const std::string topic_magnets;
    static const std::string topic_switches;

    // firmata::serial_port device;
    // firmata::client arduino;
    firmata::debounce debounce;
    const std::vector<firmata::pin*> switches;
    const std::vector<firmata::pin*> magnets;
    const unsigned ola_universe;
    ola::client::StreamingClient ola_client;
    const std::string qlab_hostname;
    const std::uint16_t qlab_port;
    const std::string full_topic_magnets;
    const std::string full_topic_switches;
    std::vector<std::uint8_t> switch_states;
    std::vector<std::uint8_t> magnet_values;
    std::vector<bool> riddle_solved;

    std::uint8_t pin_to_switch_index(firmata::pin *sw);
    void pin_change_handler(firmata::pin *sw, std::uint8_t state);
    void switch_change_handler(std::uint8_t switch_index, std::uint8_t state);
    void publish_switch_states();
    void publish_magnet_values();
    void send_qlab_command(const std::string &cmd);
    void reset();

    virtual void connection_established_handler() override;
    virtual bool interpret_msg(const mqtt::buffer &topic, const mqtt::buffer &content) override;

public:
    /**
     * @brief number of switches and magnets respectively
     */
    static const unsigned num_of_switches;

    MQTTRoomControl(boost::asio::io_context &ioc,
                    const std::string &broker_hostname,
                    std::uint16_t broker_port,
                    const std::string &node_name,
                    // const std::string &serial_device,
                    // const firmata::baud_rate &baud,
                    const std::vector<firmata::pin*> &switches,
                    const std::vector<firmata::pin*> &magnets,
                    unsigned ola_universe,
                    const std::string &qlab_hostname,
                    std::uint16_t qlab_port);

    virtual void start() override;
};

#endif // MQTTROOMCONTROL_HPP
