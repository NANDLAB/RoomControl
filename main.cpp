#include <iostream>
#include "firmata/client.hpp"
#include "firmata/serial_port.hpp"
#include "firmata/debounce.hpp"
#include "MQTTRoomControl.hpp"
#include <cstdio>
#include <iomanip>
#include <ios>
#include <exception>

int main(int argc, char *argv[])
{
    try {
        asio::io_context ioc;

        const char *broker_host = "localhost";
        std::uint16_t broker_port = 1883;
        const char *qlab_host = "Lockeds-Mini";  // 192.168.178.41
        std::uint16_t qlab_port = 53000;
        const char *arduino_device = "/dev/ttyUSB0";
        unsigned long long arduino_baud = 115200;
        unsigned ola_universe = 0;

        // Parse command line arguments
        if (argc >= 2) {
            std::ostringstream help_builder;
            const int warg = 18;
            const int wval = 12;
            help_builder << std::left << "Usage: " << argv[0] << " [OPTION]..."
                         << "\nDefault arguments are shown in brackets."
                         << "\n -h, " << std::setw(warg) << "--help " << std::setw(wval) << "Show this help."
                         << "\n -B, " << std::setw(warg) << "--broker-host " << std::setw(wval) << "<hostname> " << "[" << broker_host << "]"
                         << "\n -b, " << std::setw(warg) << "--broker-port " << std::setw(wval) << "<port> " << "[" << broker_port << "]"
                         << "\n -Q, " << std::setw(warg) << "--qlab-host " << std::setw(wval) << "<hostname> " << "[" << qlab_host << "]"
                         << "\n -q, " << std::setw(warg) << "--qlab-port " << std::setw(wval) << "<port> " << "[" << qlab_port << "]"
                         << "\n -u, " << std::setw(warg) << "--ola-universe" << std::setw(wval) << "<universe> " << "[" << ola_universe << "]"
                         << "\n -A, " << std::setw(warg) << "--arduino1-device" << std::setw(wval) << "<path> " << "[" << arduino_device << "]"
                         << "\n -a, " << std::setw(warg) << "--arduino1-baud" << std::setw(wval) << "<baudrate> " << "[" << arduino_baud << "]"
                         ;
            const std::string help = help_builder.str();

            for (const char * const * argvp = argv + 1; *argvp; argvp++) {
                if (!std::strcmp(*argvp, "-h") || !std::strcmp(*argvp, "--help")) {
                    std::cout << help << std::endl;
                    std::exit(EXIT_SUCCESS);
                }
                else if (!std::strcmp(*argvp, "-B") || !std::strcmp(*argvp, "--broker-host")) {
                    argvp++;
                    if (*argvp) {
                        broker_host = *argvp;
                    }
                    else {
                        std::cout << "Broker hostname expected!" << std::endl;
                        std::exit(EXIT_FAILURE);
                    }
                }
                else if (!std::strcmp(*argvp, "-b") || !std::strcmp(*argvp, "--broker-port")) {
                    argvp++;
                    if (*argvp) {
                        int port;
                        int ret = std::sscanf(*argvp, "%d", &port);
                        if (!ret) {
                            std::cout << "Broker port is not an integer!" << std::endl;
                            std::exit(EXIT_FAILURE);
                        }
                        if (port > 0x00 && port <= 0xFFFF) {
                            broker_port = port;
                        }
                        else {
                            std::cout << "Broker port out of range!" << std::endl;
                            std::exit(EXIT_FAILURE);
                        }
                    }
                    else {
                        std::cout << "Broker port expected!" << std::endl;
                        std::exit(EXIT_FAILURE);
                    }
                }
                else if (!std::strcmp(*argvp, "-Q") || !std::strcmp(*argvp, "--qlab-host")) {
                    argvp++;
                    if (*argvp) {
                        qlab_host = *argvp;
                    }
                    else {
                        std::cout << "QLab hostname expected!" << std::endl;
                        std::exit(EXIT_FAILURE);
                    }
                }
                else if (!std::strcmp(*argvp, "-q") || !std::strcmp(*argvp, "--qlab-port")) {
                    argvp++;
                    if (*argvp) {
                        int port;
                        int ret = std::sscanf(*argvp, "%d", &port);
                        if (!ret) {
                            std::cout << "QLab port is not an integer!" << std::endl;
                            std::exit(EXIT_FAILURE);
                        }
                        if (port > 0x00 && port <= 0xFFFF) {
                            qlab_port = port;
                        }
                        else {
                            std::cout << "QLab port out of range!" << std::endl;
                            std::exit(EXIT_FAILURE);
                        }
                    }
                    else {
                        std::cout << "QLab port expected!" << std::endl;
                        std::exit(EXIT_FAILURE);
                    }
                }
                else if (!std::strcmp(*argvp, "-A") || !std::strcmp(*argvp, "--arduino1-device")) {
                    argvp++;
                    if (*argvp) {
                        arduino_device = *argvp;
                    }
                    else {
                        std::cout << "Arduino 1 device expected!" << std::endl;
                        std::exit(EXIT_FAILURE);
                    }
                }
                else if (!std::strcmp(*argvp, "-a") || !std::strcmp(*argvp, "--arduino1-baud")) {
                    argvp++;
                    if (*argvp) {
                        long long baud;
                        int ret = std::sscanf(*argvp, "%lld", &baud);
                        if (!ret) {
                            std::cout << "Arduino 1 baud is not an integer!" << std::endl;
                            std::exit(EXIT_FAILURE);
                        }
                        if (baud < 0) {
                            std::cout << "Arduino 1 baud cannot be negative!" << std::endl;
                            std::exit(EXIT_FAILURE);
                        }
                        arduino_baud = baud;
                    }
                    else {
                        std::cout << "Arduino 1 device expected!" << std::endl;
                        std::exit(EXIT_FAILURE);
                    }
                }
                else if (!std::strcmp(*argvp, "-u") || !std::strcmp(*argvp, "--ola-universe")) {
                    argvp++;
                    if (*argvp) {
                        int u;
                        int ret = std::sscanf(*argvp, "%d", &u);
                        if (!ret) {
                            std::cout << "OLA universe not given as an integer!" << std::endl;
                            std::exit(EXIT_FAILURE);
                        }
                        if (u >= 0) {
                            ola_universe = u;
                        }
                        else {
                            std::cout << "OLA universe cannot be negative!" << std::endl;
                            std::exit(EXIT_FAILURE);
                        }
                    }
                    else {
                        std::cout << "OLA universe number expected!" << std::endl;
                        std::exit(EXIT_FAILURE);
                    }
                }
                else {
                    std::cout << "Unknown option " << std::quoted(*argvp) << "!" << std::endl;
                    std::exit(EXIT_FAILURE);
                }
            }
        }

        std::cout << "Broker Hostname: " << broker_host
                  << "\nBroker Port: " << broker_port
                  << "\nQLab Hostname: " << qlab_host
                  << "\nQLab Port: " << qlab_port
                  << "\nArduino 1 Device: " << arduino_device
                  << "\nArduino 1 Baudrate: " << arduino_baud
                  << "\nOLA universe: " << ola_universe
                  << std::endl;

        // Arduino 1
        std::cout << "Connecting to Firmata Arduino..." << std::endl;
        firmata::serial_port device1(ioc, arduino_device);
        device1.set(static_cast<firmata::baud_rate>(arduino_baud));
        firmata::client ard1(device1);
        std::cout << "Connected to Firmata Arduino." << std::endl;
        // Arduino 2
        /*
        firmata::serial_port device2(ioc, "/dev/ttyUSB1");
        device2.set(115200_baud);
        firmata::client ard2(device2);
        */
        const std::vector<firmata::pin*> switches = {
            &(ard1.pin(32)),
            &(ard1.pin(33)),
            &(ard1.pin(25)),
            &(ard1.pin(26)),
            &(ard1.pin(27)),
            &(ard1.pin(18)),
            &(ard1.pin( 4)),
            &(ard1.pin(13))
        };
        const std::vector<firmata::pin*> magnets = {
            &(ard1.pin(23)),
            &(ard1.pin(22)),
            &(ard1.pin(19)),
            &(ard1.pin( 5)),
            &(ard1.pin(14)),
            &(ard1.pin(12)),
            &(ard1.pin( 2)),
            &(ard1.pin(15))
        };
        MQTTRoomControl mqtt_node(ioc, broker_host, broker_port, "control",
                                  switches, magnets, ola_universe, qlab_host, qlab_port);
        mqtt_node.start();
        ioc.run();
        return 0;
    }
    catch (const std::exception &e) {
        std::cout << "Terminating due to exception: " << e.what() << std::endl;
        return 1;
    }
}
